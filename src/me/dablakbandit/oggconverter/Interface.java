package me.dablakbandit.oggconverter;
import java.awt.Desktop;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.AbstractButton;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;

import javax.swing.JTextPane;
import javax.swing.JProgressBar;

import java.awt.Color;

import javax.swing.JFileChooser;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JMenuBar;

@SuppressWarnings("serial")
public class Interface extends JFrame {

	private JPanel contentPane;
	private JTextField txtUrl, txtSpeed, txtState, txtEta, txtFile, txtSize, txtDestination;
	private JTextPane txtOutput;
	private JProgressBar progressBar;
	private ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnConvert_1, btnSelect;
	private JLabel lblEta, lblSpeed, lblFile;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Interface() {
		setTitle("Audio Converter/Downloader");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 310);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnConvert_1 = new JButton("Convert");
		btnConvert_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				go();
			}
		});

		btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openSelect();
			}
		});
		btnSelect.setBounds(351, 28, 73, 23);
		contentPane.add(btnSelect);

		txtFile = new JTextField();
		txtFile.setColumns(10);
		txtFile.setBounds(93, 29, 248, 20);
		contentPane.add(txtFile);
		btnConvert_1.setBounds(315, 146, 109, 23);
		contentPane.add(btnConvert_1);

		lblFile = new JLabel("File:");
		lblFile.setBounds(10, 32, 46, 14);
		contentPane.add(lblFile);

		txtOutput = new JTextPane();
		txtOutput.setEditable(false);
		txtOutput.setBounds(93, 212, 331, 48);
		contentPane.add(txtOutput);

		JLabel lblOutput = new JLabel("Output:");
		lblOutput.setBounds(10, 230, 46, 14);
		contentPane.add(lblOutput);

		progressBar = new JProgressBar();
		progressBar.setBounds(93, 150, 212, 14);
		contentPane.add(progressBar);

		txtSize = new JTextField();
		txtSize.setBackground(Color.WHITE);
		txtSize.setEditable(false);
		txtSize.setColumns(10);
		txtSize.setBounds(134, 115, 78, 20);
		contentPane.add(txtSize);

		JLabel lblSize = new JLabel("Size:");
		lblSize.setBounds(93, 118, 46, 14);
		contentPane.add(lblSize);

		txtDestination = new JTextField();
		txtDestination.setBackground(Color.WHITE);
		txtDestination.setColumns(10);
		txtDestination.setBounds(93, 180, 248, 20);
		contentPane.add(txtDestination);

		JLabel lblDestination = new JLabel("Destination:");
		lblDestination.setBounds(10, 183, 73, 14);
		contentPane.add(lblDestination);

		JLabel lblProgress = new JLabel("Progress:");
		lblProgress.setBounds(10, 150, 58, 14);
		contentPane.add(lblProgress);

		JLabel lblCodec = new JLabel("Codec:");
		lblCodec.setBounds(10, 60, 58, 14);
		contentPane.add(lblCodec);

		JRadioButton btnmp3 = new JRadioButton("mp3");
		buttonGroup.add(btnmp3);
		btnmp3.setBounds(146, 56, 51, 23);
		contentPane.add(btnmp3);

		JRadioButton btnac3 = new JRadioButton("ac3");
		buttonGroup.add(btnac3);
		btnac3.setBounds(199, 56, 51, 23);
		contentPane.add(btnac3);

		JRadioButton btnaac = new JRadioButton("aac");
		buttonGroup.add(btnaac);
		btnaac.setBounds(305, 56, 51, 23);
		contentPane.add(btnaac);

		JRadioButton btnm4a = new JRadioButton("m4a");
		buttonGroup.add(btnm4a);
		btnm4a.setBounds(252, 56, 51, 23);
		contentPane.add(btnm4a);

		JRadioButton btnogg = new JRadioButton("ogg");
		btnogg.setSelected(true);
		buttonGroup.add(btnogg);
		btnogg.setBounds(93, 56, 51, 23);
		contentPane.add(btnogg);

		txtSpeed = new JTextField();
		txtSpeed.setEditable(false);
		txtSpeed.setVisible(false);
		txtSpeed.setColumns(10);
		txtSpeed.setBackground(Color.WHITE);
		txtSpeed.setBounds(278, 115, 78, 20);
		contentPane.add(txtSpeed);

		lblSpeed = new JLabel("Speed:");
		lblSpeed.setVisible(false);
		lblSpeed.setBounds(222, 118, 51, 14);
		contentPane.add(lblSpeed);

		JLabel lblState = new JLabel("State:");
		lblState.setBounds(93, 87, 46, 14);
		contentPane.add(lblState);

		txtState = new JTextField();
		txtState.setEditable(false);
		txtState.setColumns(10);
		txtState.setBackground(Color.WHITE);
		txtState.setBounds(134, 84, 78, 20);
		contentPane.add(txtState);

		lblEta = new JLabel("ETA:");
		lblEta.setVisible(false);
		lblEta.setBounds(222, 86, 51, 14);
		contentPane.add(lblEta);

		txtEta = new JTextField();
		txtEta.setEditable(false);
		txtEta.setVisible(false);
		txtEta.setColumns(10);
		txtEta.setBackground(Color.WHITE);
		txtEta.setBounds(278, 84, 78, 20);
		contentPane.add(txtEta);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 21);
		contentPane.add(menuBar);

		JButton btnConvert = new JButton("Convert");
		btnConvert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchToConvert();
			}
		});
		btnConvert.setOpaque(true);
		btnConvert.setContentAreaFilled(false);
		btnConvert.setBorderPainted(false);
		btnConvert.setFocusable(false);
		menuBar.add(btnConvert);

		JButton btnDownload = new JButton("Download");
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchToDownload();
			}
		});
		btnDownload.setOpaque(true);
		btnDownload.setContentAreaFilled(false);
		btnDownload.setBorderPainted(false);
		btnDownload.setFocusable(false);
		menuBar.add(btnDownload);

		txtUrl = new JTextField();
		txtUrl.setVisible(false);
		txtUrl.setBounds(93, 29, 331, 20);
		contentPane.add(txtUrl);
		txtUrl.setColumns(10);

		btnOpen = new JButton("Open");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				open();
			}
		});
		btnOpen.setBounds(351, 179, 73, 23);
		contentPane.add(btnOpen);
	}

	public WorkingThread wt;

	private File currentDir = new File("");
	private File ffmpeg = new File(currentDir.getAbsoluteFile(), File.separator + "FFMPEG" + File.separator + "bin" + File.separator + "ffmpeg.exe");

	public void go(){
		if(wt!=null&&wt.getRunning())return;
		wt = new WorkingThread();
		Thread t = new Thread(wt);
		t.start();
	}

	public boolean isDownload(){
		return btnConvert_1.getText().equals("Download");
	}

	public void switchToDownload(){
		if(wt!=null&&wt.getRunning())return;
		if(isDownload())return;
		txtEta.setVisible(true);
		lblEta.setVisible(true);
		txtSpeed.setVisible(true);
		lblSpeed.setVisible(true);
		txtUrl.setVisible(true);
		lblFile.setText("URL:");
		btnConvert_1.setText("Download");
		btnSelect.setVisible(false);
		txtFile.setVisible(false);
	}

	public boolean isConvert(){
		return btnConvert_1.getText().equals("Convert");
	}

	public void switchToConvert(){
		if(wt!=null&&wt.getRunning())return;
		if(isConvert())return;
		txtEta.setVisible(false);
		lblEta.setVisible(false);
		txtSpeed.setVisible(false);
		lblSpeed.setVisible(false);
		txtUrl.setVisible(false);
		lblFile.setText("File:");
		btnConvert_1.setText("Convert");
		btnSelect.setVisible(true);
		txtFile.setVisible(true);
	}

	public void select(){
		if(wt!=null&&wt.getRunning())return;
	}

	public JFileChooser chooser = getChooser();
	private JButton btnOpen;

	public JFileChooser getChooser(){
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Audio Files", "mp3", "ac3", "m4a", "aac", "ogg", "wav");
		JFileChooser jfc = new JFileChooser();
		jfc.setFileFilter(filter);
		return jfc;
	}

	public void open(){
		File f = new File(txtDestination.getText());
		try{
			Desktop.getDesktop().browse(f.toURI());
		}catch(IOException e){}
	}

	public void openSelect(){
		int result = chooser.showOpenDialog(null);
		switch(result){
		case JFileChooser.APPROVE_OPTION:
			txtFile.setText(chooser.getSelectedFile().getAbsolutePath());
			break;
		}
	}

	private void downloadURL(){
		txtSize.setText("");
		txtDestination.setText("");
		String best = getBestFormat();
		if(best.equals("")){
			txtState.setText("Failed");
		}else{
			try{
				txtState.setText("Downloading");
				Runtime rt = Runtime.getRuntime();
				String command = "youtube-dl.exe " + txtUrl.getText() + " -f " + best + " --no-playlist --no-part --no-continue -o %(title)s.%(ext)s --ffmpeg-location \"" + ffmpeg.getAbsolutePath() + "\"";
				Process proc = rt.exec(command);
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
				String s = null;
				while((s = stdInput.readLine())!=null){
					process(s);
				}
				while((s = stdError.readLine())!=null){
					process(s);
				}
				progressBar.setValue(100);
				txtEta.setText("");
				txtSpeed.setText("");
				convert();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public void convert(){
		try{
			File f2 = new File(currentDir.getAbsoluteFile(), txtDestination.getText());
			String type = getSelectedButtonText(buttonGroup);
			if(f2.exists()&&!type.equals("m4a")){
				Runtime rt = Runtime.getRuntime();
				txtState.setText("Converting");
				String file = f2.getAbsolutePath();
				String file1 = file.substring(0, file.length()-3) + type;
				String command = ffmpeg.getAbsolutePath() + " -i \"" + file + "\" -y -acodec " + getFormat(type) + " \"" + file1 + "\"";
				Process proc = rt.exec(command);
				BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
				String s = null;
				double duration = 0.0;
				while((s = stdError.readLine())!=null){
					txtOutput.setText(s);
					if(duration==0.0){
						duration = checkDuration(s);
					}else{
						double d = checkProgress(s);
						if(d!=0.0){
							int i = (int)(d/duration*100);
							progressBar.setValue(i);
						}
					}
				}
				progressBar.setValue(100);
				f2.delete();
				File f3 = new File(file1);
				txtSize.setText(formatSize(f3.length()));
				txtDestination.setText(f3.getAbsolutePath());
			}
			txtState.setText("Finished");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void convertFile(){
		txtSize.setText("");
		txtDestination.setText("");
		txtEta.setText("");
		txtSpeed.setText("");
		try{
			File f2 = new File(txtFile.getText());
			String type = getSelectedButtonText(buttonGroup);
			if(f2.exists()&&!type.equals(getExtension(f2))){
				Runtime rt = Runtime.getRuntime();
				txtState.setText("Converting");
				String file = f2.getAbsolutePath();
				String file1 = file.substring(0, file.length()-3) + type;
				String command = ffmpeg.getAbsolutePath() + " -i \"" + file + "\" -y -acodec " + getFormat(type) + " \"" + file1 + "\"";
				Process proc = rt.exec(command);
				BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
				String s = null;
				double duration = 0.0;
				while((s = stdError.readLine())!=null){
					txtOutput.setText(s);
					if(duration==0.0){
						duration = checkDuration(s);
					}else{
						double d = checkProgress(s);
						if(d!=0.0){
							int i = (int)(d/duration*100);
							progressBar.setValue(i);
						}
					}
				}
				progressBar.setValue(100);
				File f3 = new File(file1);
				txtSize.setText(formatSize(f3.length()));
				txtDestination.setText(f3.getAbsolutePath());
			}
			txtState.setText("Finished");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public String getExtension(File f){
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');
		if(i > 0 && i < s.length() - 1){
			ext = s.substring(i+1).toLowerCase();
		}
		return ext;
	}

	public String formatSize(double size){
		return String.format("%.2f", new Object[]{size/1024/1024}) + "MiB";
	}

	public String getSelectedButtonText(ButtonGroup buttonGroup){
		for(Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();){
			AbstractButton button = buttons.nextElement();
			if(button.isSelected()){
				return button.getText();
			}
		}
		return null;
	}

	public String getFormat(String s){
		switch(s){
		case "m4a":break;
		case "aac":return "aac";
		case "ac3":return "ac3";
		case "mp3":return "mp3";
		case "ogg":return "libvorbis";
		}
		return "m4a";
	}

	public void process(String s){
		try{
			if(s.startsWith("[download]")){
				String s1 = s.replace("[download]", "");
				while(s1.startsWith(" ")){
					s1 = s1.substring(1);
				}
				if(s1.startsWith("Destination:")){
					txtDestination.setText(s1.replace("Destination: ", ""));
				}else if(s1.startsWith("100%")){
					progressBar.setValue(100);
				}else if(s1.startsWith("Resuming")){

				}else{
					String[] a = s1.split(" ");
					int i = (int)Math.round(Double.parseDouble(a[0].replaceAll("%", "")));
					if(txtSize.getText().equals("")){
						txtSize.setText(a[2]);
					}
					progressBar.setValue(i);
					txtSpeed.setText(a[4]);
					txtEta.setText(a[6]);
				}
			}else if(s.startsWith("[ffmpeg]")){
				String s1 = s.replace("[ffmpeg]", "");
				while(s1.startsWith(" ")){
					s1 = s1.substring(1);
				}
				if(s1.startsWith("Destination:")){
					txtDestination.setText(s1.replace("Destination:", ""));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		txtOutput.setText(s);
	}

	public double checkDuration(String s){
		if(s.startsWith("  Duration: ")){
			s = s.replaceFirst("  Duration: ", "");
			String a[] = s.split(", ");
			String b[] = a[0].split(":");
			return Integer.parseInt(b[0]) * 3600 + Integer.parseInt(b[1]) * 60 + Double.parseDouble(b[2]);
		}
		return 0.0;
	}

	public double checkProgress(String s){
		if(s.startsWith("size=")){
			while(s.contains("  "))s = s.replaceAll("  ", " ");
			String a[] = s.split(" ");
			String b[] = a[2].split("=")[1].split(":");
			return Integer.parseInt(b[0]) * 3600 + Integer.parseInt(b[1]) * 60 + Double.parseDouble(b[2]);
		}
		return 0.0;
	}

	public String getBestFormat(){
		try{
			txtState.setText("Checking");
			Runtime rt = Runtime.getRuntime();
			String command = "youtube-dl.exe " + txtUrl.getText() + " --no-playlist -F";
			Process proc = rt.exec(command);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String s = null;
			String b = "";
			while((s = stdInput.readLine()) != null){
				txtOutput.setText(s);
				try{
					while(s.contains("  "))s = s.replaceAll("  ", " ");
					String a[] = s.split(" ");
					Integer.parseInt(a[0]);
					if(a[1].equals("m4a")&&a[2].equals("audio")){
						b = a[0];
					}
				}catch(Exception e){}
			}
			return b;
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}

	public class WorkingThread implements Runnable{

		public boolean running = false;

		@Override
		public void run() {
			running = true;
			btnConvert_1.setEnabled(false);
			txtDestination.setEditable(false);
			if(isDownload())downloadURL();
			if(isConvert())convertFile();
			btnConvert_1.setEnabled(true);
			txtDestination.setEditable(true);
			running = false;
		}

		public boolean getRunning(){
			return running;
		}
	}
}
